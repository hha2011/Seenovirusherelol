/* autogenerated by Processing revision 1283 on 2023-01-24 */
import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

public class yayimsmart extends PApplet {

float x;
float y;

 public void setup() {
  /* size commented out by preprocessor */;
  x = width / 2;
  y = height / 2;
  windowTitle("yammilikethis");
}

 public void draw() {
  background(200);
  ellipse(x, y, 20, 20);
  x += cos(atan2(mouseY - y, mouseX - x));
  y += sin(atan2(mouseY - y, mouseX - x));
}


  public void settings() { size(300, 200); }

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "yayimsmart" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
