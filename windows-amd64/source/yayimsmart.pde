float x;
float y;

void setup() {
  size(300, 200);
  x = width / 2;
  y = height / 2;
  windowTitle("yammilikethis");
}

void draw() {
  background(200);
  ellipse(x, y, 20, 20);
  x += cos(atan2(mouseY - y, mouseX - x));
  y += sin(atan2(mouseY - y, mouseX - x));
}
